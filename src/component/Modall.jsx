import React, { Component } from "react";
import { Modal, Button } from "react-bootstrap";

export default class Modall extends Component {
  closeModal = () => {
    this.props.closeModal();
  };
  render(props) {
    const job = this.props.person.job.map((m, index) => (
      <li key={index}>{m}</li>
    ));
    return (
      <div>
        <Modal show={this.props.isModal}>
          <Modal.Header closeButton onClick={this.closeModal}>
            <Modal.Title>{this.props.person.name}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h3>Gender : {this.props.person.gender} </h3>
            <h3>Job</h3>
            <ul>{job}</ul>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.closeModal}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}
