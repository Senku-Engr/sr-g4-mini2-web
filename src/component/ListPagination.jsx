import React, { Component } from "react";
import { Pagination } from "react-bootstrap";

export default class ListPagination extends Component {
  appendpagination = () => {
    this.props.appendpagination(this.props.data);
  };
  render(props) {
    return (
      <Pagination.Item
        onClick={this.appendpagination}
        active={this.props.forActive === this.props.data ? true : false}
      >
        {this.props.data}
      </Pagination.Item>
    );
  }
}
