import React, { Component } from "react";
import { Pagination } from "react-bootstrap";
import ListPagination from "./ListPagination";

export default class Paginationn extends Component {
  constructor() {
    super();
    this.appendpagination = this.appendpagination.bind(this);
    this.state = {
      arr: [],
    };
  }
  appendpagination = (e) => {
    this.props.pagi(e);
  };

  render(props) {
    const pagination = this.props.alldata.map((i, index) => (
      <ListPagination
        data={i}
        appendpagination={this.appendpagination}
        key={index}
        forActive={this.props.forActive}
      />
    ));

    const pp = (
      <Pagination>
        {/* {pagination <= 0 ? null : <Pagination.Prev />} */}

        {pagination}
        {/* {pagination <= 0 ? null : <Pagination.Next />} */}
      </Pagination>
    );

    return <div>{pp}</div>;
  }
}
