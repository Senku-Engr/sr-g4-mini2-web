import React, { Component } from "react";
import { Button } from "react-bootstrap";
import "moment/locale/km";
const moment = require("moment");
moment.locale("km");

export default class List extends Component {
  constructor() {
    super();
    this.btnView = this.btnView.bind(this);
    this.btnUpdate = this.btnUpdate.bind(this);
    this.btnDelete = this.btnDelete.bind(this);
  }
  btnView = () => {
    this.props.btnView(this.props.person.id);
  };
  btnUpdate = () => {
    this.props.btnUpdate(this.props.person.id);
  };
  btnDelete = () => {
    this.props.btnDelete(this.props.person.id);
  };

  render(props) {
    const job = this.props.person.job.map((m, index) => (
      <li key={index}>{m}</li>
    ));

    return (
      <tr>
        <td>{this.props.person.id}</td>
        <td>{this.props.person.name}</td>
        <td>{this.props.person.age}</td>
        <td>{this.props.person.gender}</td>
        <td>
          <ul>{job}</ul>
        </td>
        <td>{moment(this.props.person.create).fromNow()}</td>

        <td>{moment(this.props.person.update).fromNow()}</td>
        <td>
          <Button className="btn btn-info" onClick={this.btnView}>
            view
          </Button>
          <Button
            className="btn btn-warning"
            onClick={this.btnUpdate}
            disabled={this.props.disUpdate === true ? true : false}
          >
            update
          </Button>
          <Button
            className="btn btn-danger"
            onClick={this.btnDelete}
            disabled={this.props.disDelete === true ? true : false}
          >
            delete
          </Button>
        </td>
      </tr>
    );
  }
}
