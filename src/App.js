import React from "react";
// import logo from "./logo.svg";
import "./App.css";
import Personalinfo from "./component/Personalinfo";

function App() {
  return (
    <div className="App">
      <Personalinfo />
    </div>
  );
}

export default App;
